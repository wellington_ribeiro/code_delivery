<?php

namespace CodeDelivery\Repositories;

use CodeDelivery\Validators\UserValidator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace CodeDelivery\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
