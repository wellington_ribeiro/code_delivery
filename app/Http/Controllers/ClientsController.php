<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminClientRequest;
use CodeDelivery\Repositories\ClientRepository;
use CodeDelivery\Http\Requests;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\ClientService;

class ClientsController extends Controller
{
    private $clientRepository;
    private $userRepository;
    private $clientService;

    public function __construct(ClientRepository $clientRepository, UserRepository $userRepository, ClientService $clientService)
    {
        $this->clientRepository = $clientRepository;
        $this->userRepository = $userRepository;
        $this->clientService = $clientService;
    }

    public function index()
    {
        $clients = $this->clientRepository->paginate();
        return view('admin.clients.index',compact('clients'));
    }

    public function create()
    {
        return view('admin.clients.create');
    }
    
    public function store(AdminClientRequest $request)
    {
        $data = $request->all();
        $this->clientService->create($data);
        return redirect()->route('admin.clients.index');
    }
    
    public function edit($id)
    {
        $client = $this->clientRepository->with('user')->find($id);
        return view('admin.clients.edit',compact('client'));
    }

    public function update(AdminClientRequest $request,$id)
    {
        $data = $request->all();
        $this->clientService->update($data,$id);
        return redirect()->route('admin.clients.index');
    }

    public function destroy($id)
    {
        $response = $this->clientService->destroy($id);
        $redirect = redirect()->route('admin.clients.index');
        if (!$response['error']) {
            $redirect->with('message',$response['message']);
        } else {
            $redirect->with('error',$response['message']);
        }
        return $redirect;
    } 
}
