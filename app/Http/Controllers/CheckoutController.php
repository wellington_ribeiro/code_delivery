<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\CheckoutRequest;
use CodeDelivery\Http\Requests\CustomerOrderRequest;
use CodeDelivery\Repositories\CategoryRepository;
use CodeDelivery\Http\Requests;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\CategoryService;
use CodeDelivery\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var OrderService
     */
    private $orderService;

    public function __construct(
            OrderRepository $orderRepository,
            UserRepository $userRepository,
            ProductRepository $productRepository,
            OrderService $orderService)
    {
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->orderService = $orderService;
    }

    public function index()
    {
        $client_id = $this->userRepository->find(Auth::user()->id)->client->id;
        $orders = $this->orderRepository->scopeQuery(function ($query) use ($client_id) {
            return $query->where('client_id','=',$client_id);
        })->paginate();
        return view('customer.orders.index',compact('orders'));
    }
    
    public function create()
    {
        $products = $this->productRepository->select();
        return view('customer.orders.create',compact('products'));
    }

    public function store(CheckoutRequest $request)
    {
        $data = $request->all();
        $client_id = $this->userRepository->find(Auth::user()->id)->client->id;
        $data['client_id'] = $client_id;
        $this->orderService->create($data);
        return redirect()->route('customer.orders.index');
    }
}
