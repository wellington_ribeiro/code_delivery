<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminProductRequest;
use CodeDelivery\Repositories\CategoryRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Http\Requests;
use CodeDelivery\Services\ProductService;

class ProductsController extends Controller
{
    private $repository;
    private $categoryRepository;
    private $productService;

    public function __construct(ProductRepository $repository, CategoryRepository $categoryRepository, ProductService $productService)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
        $this->productService = $productService;
    }

    public function index()
    {
        $products = $this->repository->paginate();
        return view('admin.products.index',compact('products'));
    }

    public function create()
    {
        $categories = $this->categoryRepository->lists('name','id');
        return view('admin.products.create',compact('categories'));
    }
    
    public function store(AdminProductRequest $request)
    {
        $data = $request->all();
        $this->repository->create($data);
        return redirect()->route('admin.products.index');
    }
    
    public function edit($id)
    {
        $product = $this->repository->find($id);
        $categories = $this->categoryRepository->lists('name','id');
        return view('admin.products.edit',compact('product','categories'));
    }

    public function update(AdminProductRequest $request,$id)
    {
        $data = $request->all();
        $this->repository->update($data,$id);
        return redirect()->route('admin.products.index');
    }
    
    public function destroy($id)
    {
        $response = $this->productService->destroy($id);
        $redirect = redirect()->route('admin.products.index');
        if (!$response['error']) {
            $redirect->with('message',$response['message']);
        } else {
            $redirect->with('error',$response['message']);
        }
        return $redirect;
    }
}
