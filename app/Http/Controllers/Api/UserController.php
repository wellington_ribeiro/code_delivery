<?php

namespace CodeDelivery\Http\Controllers\Api;

use CodeDelivery\Http\Controllers\Controller;
use CodeDelivery\Http\Requests;
use CodeDelivery\Repositories\UserRepository;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use Mockery\CountValidator\Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAuthenticatedUser()
    {
        return $this->userRepository->find(Authorizer::getResourceOwnerId());
    }

    public function registerUser(Request $request)
    {
        try {
            $user_data = $request->only('email','name','password');
            $user_data['password'] = Hash::make($user_data['password']);
            $user = $this->userRepository->create($user_data);
            $data = $this->userRepository->skipPresenter(false)->find($user->id);
            return response()->json([
                'message'=>'User registred',
                'data'   => $data
            ]);
        } catch (ValidatorException $e) {
            return response()->json([
                'error'   =>true,
                'message' =>$e->getMessageBag()
            ]);
        }
    }

    public function sendPasswordResetToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors()
            ]);
        }

        try {
            $email = $request->get('email');
            $token = strtoupper(str_random(4));
            $password_reset = DB::table('password_resets')->where('email',$email)->first();

            if (!$password_reset) {
                DB::table('password_resets')->insert(['email' => $email, 'token' => $token]);
            } else {
                DB::table('password_resets')->where('email',$email)->update(['token' => $token]);
            }
            
            Mail::send('emails.reminder',['token' => $token], function($message) use ($email){
                $message->to($email,'')->subject('CodeDelivery - Recuperação de senha');
            });

            return response()->json([
                'message'=>'Email sended'
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'error'=>true,
                'message'=>trans($e->getMessage())
            ]);
        }
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
            'token' => 'required',
            'password' => 'required|min:4'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors()
            ]);
        }

        try {
            $credentials = $request->only('email','token');
            $password_reset = DB::table('password_resets')->where($credentials)->first();

            if (!$password_reset) {
                throw new \Exception('Invalid credentials');
            }

            DB::table('users')->where('email',$credentials['email'])->update([
                'password' => Hash::make($request->get('password'))
            ]);
            DB::table('password_resets')->where($credentials)->delete();

            return response()->json([
                'message'=>'Password updated'
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'error'=>true,
                'message'=>trans($e->getMessage())
            ]);
        }
    }

}
