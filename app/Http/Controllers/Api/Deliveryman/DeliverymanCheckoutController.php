<?php

namespace CodeDelivery\Http\Controllers\Api\Deliveryman;

use CodeDelivery\Http\Controllers\Controller;
use CodeDelivery\Http\Requests;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\OrderService;
use Illuminate\Http\Request;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class DeliverymanCheckoutController extends Controller
{
    private $orderRepository;
    private $userRepository;
    private $orderService;

    private $relationships = ['cupom','items','client'];

    public function __construct(
            OrderRepository $orderRepository,
            UserRepository $userRepository,
            OrderService $orderService)
    {
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->orderService = $orderService;
    }

    public function index()
    {
        $deliveryman_id = Authorizer::getResourceOwnerId();
        $orders = $this->orderRepository
            ->skipPresenter(false)
            ->with($this->relationships)
            ->scopeQuery(function ($query) use ($deliveryman_id) {
            return $query->where('user_deliveryman_id','=',$deliveryman_id);
        })->paginate();
        return $orders;
    }
    
    public function show($id)
    {
        $deliveryman_id = Authorizer::getResourceOwnerId();
        return $this->orderRepository
            ->skipPresenter(false)
            ->findByIdAndDeliveryman($id,$deliveryman_id);
    }

    public function updateStatus(Request $request,$id)
    {
        $deliveryman_id = Authorizer::getResourceOwnerId();
        $order = $this->orderService->updateStatus($id,$deliveryman_id,$request->get('status'));
        if ($order) {
            return $this->orderRepository->find($order->id);
        }
        abort(400,"Order not found");
    }
}
