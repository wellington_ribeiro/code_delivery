<?php

namespace CodeDelivery\Http\Controllers\Api\Client;

use CodeDelivery\Http\Controllers\Controller;
use CodeDelivery\Http\Requests\CheckoutRequest;
use CodeDelivery\Http\Requests\CustomerOrderRequest;
use CodeDelivery\Http\Requests;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\OrderService;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class ClientCheckoutController extends Controller
{
    private $orderRepository;
    private $userRepository;
    private $orderService;

    private $relationships = ['cupom','items','client'];

    public function __construct(
            OrderRepository $orderRepository,
            UserRepository $userRepository,
            OrderService $orderService)
    {
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->orderService = $orderService;
    }

    public function index()
    {
        $user_id = Authorizer::getResourceOwnerId();
        $client_id = $this->userRepository->find($user_id)->client->id;
        $orders = $this->orderRepository
            ->skipPresenter(false)
            ->with($this->relationships)
            ->scopeQuery(function ($query) use ($client_id) {
            return $query->where('client_id','=',$client_id);
        })->paginate();
        return $orders;
    }

    public function store(CheckoutRequest $request)
    {
        $data = $request->all();
        $user_id = Authorizer::getResourceOwnerId();
        $client_id = $this->userRepository->find($user_id)->client->id;
        $data['client_id'] = $client_id;
        $order = $this->orderService->create($data);
        return $this->orderRepository
            ->skipPresenter(false)
            ->with($this->relationships)
            ->find($order->id);
    }
    
    public function show($id)
    {
        return $this->orderRepository
            ->skipPresenter(false)
            ->with($this->relationships)
            ->find($id);
    }
}
