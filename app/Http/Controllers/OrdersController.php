<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminOrderRequest;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Http\Requests;
use CodeDelivery\Repositories\UserRepository;

class OrdersController extends Controller
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function index()
    {
        $orders = $this->orderRepository->paginate();
        return view('admin.orders.index',compact('orders'));
    }
    
    public function edit($id, UserRepository $userRepository)
    {
        $order = $this->orderRepository->find($id);
        $list_status = [ 0 => 'Pendente', 1 => 'A caminho', 2 => 'Entregue', 3 => 'Cancelado']; //todo criar tabela
        $list_deliveries = $userRepository->findWhere(['role'=>'deliveryman'])->lists('name','id');
        return view('admin.orders.edit',compact('order','list_status','list_deliveries'));
    }
    
    public function update(AdminOrderRequest $request,$id)
    {
        $data = $request->all();
        $this->orderRepository->update($data,$id);
        return redirect()->route('admin.orders.index');
    }
}
