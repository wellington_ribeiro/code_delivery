<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Http\Requests\AdminCategoryRequest;
use CodeDelivery\Repositories\CategoryRepository;
use CodeDelivery\Http\Requests;
use CodeDelivery\Services\CategoryService;

class CategoriesController extends Controller
{
    private $repository;
    private $service;

    public function __construct(CategoryRepository $repository, CategoryService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index()
    {
        $categories = $this->repository->paginate();
        return view('admin.categories.index',compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.create');
    }
    
    public function store(AdminCategoryRequest $request)
    {
        $data = $request->all();
        $this->repository->create($data);

        return redirect()->route('admin.categories.index');
    }
    
    public function edit($id)
    {
        $category = $this->repository->find($id);
        return view('admin.categories.edit',compact('category'));
    }

    public function update(AdminCategoryRequest $request,$id)
    {
        $data = $request->all();
        $this->repository->update($data,$id);

        return redirect()->route('admin.categories.index');
    }

    public function destroy($id)
    {
        $response = $this->service->destroy($id);
        $redirect = redirect()->route('admin.categories.index');
        if (!$response['error']) {
            $redirect->with('message',$response['message']);
        } else {
            $redirect->with('error',$response['message']);
        }
        return $redirect;
    }
}
