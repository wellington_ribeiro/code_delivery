<?php

namespace CodeDelivery\Http\Requests;

use CodeDelivery\Http\Requests\Request;

class AdminClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
        ];

        foreach($this->request->get('user') as $key => $val)
        {
            switch ($key) {
                case 'name':$rules['user.'.$key] = 'required|min:3';break;
                case 'email':$rules['user.'.$key] = 'required|email';break;
            }
        }

        return $rules;
    }
}
