<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'admin','as'=>'admin.','middleware'=>'auth.checkrole:admin'],function (){
    Route::get('categories', ['uses'=>'CategoriesController@index','as'=>'categories.index']);
    Route::get('categories/create', ['uses'=>'CategoriesController@create','as'=>'categories.create']);
    Route::post('categories/store', ['uses'=>'CategoriesController@store','as'=>'categories.store']);
    Route::get('categories/edit/{id}', ['uses'=>'CategoriesController@edit','as'=>'categories.edit']);
    Route::post('categories/update/{id}', ['uses'=>'CategoriesController@update','as'=>'categories.update']);
    Route::get('categories/destroy/{id}', ['uses'=>'CategoriesController@destroy','as'=>'categories.destroy']);

    Route::get('products', ['uses'=>'ProductsController@index','as'=>'products.index']);
    Route::get('products/create', ['uses'=>'ProductsController@create','as'=>'products.create']);
    Route::post('products/store', ['uses'=>'ProductsController@store','as'=>'products.store']);
    Route::get('products/edit/{id}', ['uses'=>'ProductsController@edit','as'=>'products.edit']);
    Route::post('products/update/{id}', ['uses'=>'ProductsController@update','as'=>'products.update']);
    Route::get('products/destroy/{id}', ['uses'=>'ProductsController@destroy','as'=>'products.destroy']);

    Route::get('clients', ['uses'=>'ClientsController@index','as'=>'clients.index']);
    Route::get('clients/create', ['uses'=>'ClientsController@create','as'=>'clients.create']);
    Route::post('clients/store', ['uses'=>'ClientsController@store','as'=>'clients.store']);
    Route::get('clients/edit/{id}', ['uses'=>'ClientsController@edit','as'=>'clients.edit']);
    Route::post('clients/update/{id}', ['uses'=>'ClientsController@update','as'=>'clients.update']);
    Route::get('clients/destroy/{id}', ['uses'=>'ClientsController@destroy','as'=>'clients.destroy']);

    Route::get('orders', ['uses'=>'OrdersController@index','as'=>'orders.index']);
    Route::get('orders/{id}', ['uses'=>'OrdersController@edit','as'=>'orders.edit']);
    Route::post('orders/{id}', ['uses'=>'OrdersController@update','as'=>'orders.update']);

    Route::get('cupoms', ['uses'=>'CupomsController@index','as'=>'cupoms.index']);
    Route::get('cupoms/create', ['uses'=>'CupomsController@create','as'=>'cupoms.create']);
    Route::post('cupoms/store', ['uses'=>'CupomsController@store','as'=>'cupoms.store']);
});

Route::group(['prefix'=>'customer','middleware'=>'auth.checkrole:client','as'=>'customer.'], function (){
    Route::get('orders',['uses'=>'CheckoutController@index','as'=>'orders.index']);
    Route::get('orders/create',['uses'=>'CheckoutController@create','as'=>'orders.create']);
    Route::post('orders/store',['uses'=>'CheckoutController@store','as'=>'orders.store']);
});

Route::group(['middleware'=>'cors'],function (){

    Route::post('oauth/access_token', function() {
        return Response::json(Authorizer::issueAccessToken());
    });

    Route::group(['prefix'=>'api','middleware'=>'oauth','as'=>'api.'], function (){
        Route::group(['prefix'=>'client','middleware'=>'oauth.checkrole:client','as'=>'client.'], function () {
            Route::resource('orders', 'Api\Client\ClientCheckoutController', ['except'=> ['create','edit','destroy']]);
            Route::get('products','Api\Client\ClientProductController@index');
        });

        Route::group(['prefix'=>'deliveryman','middleware'=>'oauth.checkrole:deliveryman','as'=>'deliveryman.'], function () {
            Route::resource('orders', 'Api\Deliveryman\DeliverymanCheckoutController', ['except'=> ['create','edit','destroy']]);
            Route::patch('orders/{id}/update-status',[
                'uses'=>'Api\Deliveryman\DeliverymanCheckoutController@updateStatus','as'=>'orders.update_status'
            ]);
        });

        Route::get('authenticated',['uses' => 'Api\UserController@getAuthenticatedUser', 'as' => 'user.authenticated']);
    });

    Route::group(['prefix'=>'api','as'=>'api.'], function (){
        Route::post('/register',['uses' => 'Api\UserController@registerUser', 'as' => 'user.register']);
        Route::post('/password-reminder',['uses' => 'Api\UserController@sendPasswordResetToken', 'as'=> 'user.password-reminder']);
        Route::post('/password-reset',['uses' => 'Api\UserController@resetPassword', 'as'=> 'user.password-reset']);
    });
    
});
