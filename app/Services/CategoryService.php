<?php
/**
 * Created by IntelliJ IDEA.
 * User: infatec
 * Date: 12/1/16
 * Time: 11:41 AM
 */

namespace CodeDelivery\Services;


use CodeDelivery\Models\Product;
use CodeDelivery\Repositories\CategoryRepository;
use CodeDelivery\Repositories\ClientRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Repositories\UserRepository;

class CategoryService
{

    private $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function destroy($id)
    {
        try{
            $category = $this->repository->find($id);
            $products = count($category->products);
            if ($products > 0) {
                return ['error'=>true,'message'=>"Não é possível remover esta categoria porque a mesma possui $products item(s)"];
            }
            $this->repository->delete($id);
        }
        catch (\Exception $e) {
            return ['error'=>true,'message'=>'Erro ao excluir registro.'];
        }
        return ['error'=>false,'message'=>'Registro deletado com sucesso.'];
    }
}