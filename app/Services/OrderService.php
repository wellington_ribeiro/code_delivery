<?php
/**
 * Created by IntelliJ IDEA.
 * User: infatec
 * Date: 12/1/16
 * Time: 11:41 AM
 */

namespace CodeDelivery\Services;


use CodeDelivery\Models\Order;
use CodeDelivery\Models\Product;
use CodeDelivery\Repositories\CategoryRepository;
use CodeDelivery\Repositories\ClientRepository;
use CodeDelivery\Repositories\CupomRepository;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Repositories\UserRepository;

class OrderService
{

    private $repository;

    private $cupomRepository;

    private $productRepository;

    public function __construct(OrderRepository $repository, CupomRepository $cupomRepository, ProductRepository $productRepository)
    {
        $this->repository = $repository;
        $this->cupomRepository = $cupomRepository;
        $this->productRepository = $productRepository;
    }


    public function create(array $data)
    {
        \DB::beginTransaction();
        try{
            $data['status'] = 0;
            if (isset($data['cupom_id'])) {
                unset($data['cupom_id']);
            }

            if (isset($data['cupom_code'])) {
                $cupom = $this->cupomRepository->findByField('code',$data['cupom_code'])->first();
                $data['cupom_id'] = $cupom->id;
                $cupom->used = 1;
                $cupom->save();

                unset($data['cupom_code']);
            }

            $items = $data['items'];
            unset($data['items']);

            $order = $this->repository->create($data);
            $total = 0;

            foreach ($items as $item) {
                $item['price'] = $this->productRepository->find($item['product_id'])->price;
                $order->items()->create($item);
                $total += $item['price'] * $item['amount'];
            }

            $order->total = $total;
            if (isset($cupom)) {
                $order->total = $total - $cupom->value;
            }
            $order->save();
            \DB::commit();
            return $order;
            
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
        }
    }

    public function updateStatus($id, $deliveryman_id, $status)
    {
        $order = $this->repository->findByIdAndDeliveryman($id,$deliveryman_id);
        if ($order instanceof Order) {
            $order->status = $status;
            $order->save();
            return $order;
        }
        return false;
    }
}