<?php
/**
 * Created by IntelliJ IDEA.
 * User: infatec
 * Date: 12/1/16
 * Time: 11:41 AM
 */

namespace CodeDelivery\Services;


use CodeDelivery\Repositories\ClientRepository;
use CodeDelivery\Repositories\UserRepository;

class ClientService
{
    private $repository;
    private $userRepository;

    public function __construct(ClientRepository $clientRepository, UserRepository $userRepository)
    {
        $this->repository = $clientRepository;
        $this->userRepository = $userRepository;
    }

    public function update(array $data,$id)
    {
        $this->repository->update($data,$id);
        $user = $this->repository->find($id)->user;
        $this->userRepository->update($data,$user->id);
    }

    public function create(array $data)
    {
        $data['user']['password'] = bcrypt(123456);
        $user = $this->userRepository->create($data['user']);
        $this->repository->create(array_merge($data,['user_id'=>$user->id]));
    }

    public function destroy($id)
    {
        try{
            $client = $this->repository->find($id);
            $orders = count($client->orders);
            if ($orders > 0) {
                return ['error'=>true,'message'=>"Não é possível remover este cliente porque o mesmo possui $orders pedido(s)"];
            }
            $this->repository->delete($id);
        }
        catch (\Exception $e) {
            return ['error'=>true,'message'=>'Erro ao excluir registro.'];
        }
        return ['error'=>false,'message'=>'Registro deletado com sucesso.'];
    }
}