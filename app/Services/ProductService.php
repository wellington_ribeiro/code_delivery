<?php
/**
 * Created by IntelliJ IDEA.
 * User: infatec
 * Date: 12/1/16
 * Time: 11:41 AM
 */

namespace CodeDelivery\Services;


use CodeDelivery\Models\Product;
use CodeDelivery\Repositories\ClientRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Repositories\UserRepository;

class ProductService
{

    private $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function destroy($id)
    {
        try{
            $product = $this->repository->find($id);
            $orders = count($product->orders);
            if ($orders > 0) {
                return ['error'=>true,'message'=>"Não é possível remover este item porque o mesmo está presente em $orders pedido(s)"];
            }
            $this->repository->delete($id);
        }
        catch (\Exception $e) {
            return ['error'=>true,'message'=>'Erro ao excluir registro.'];
        }
        return ['error'=>false,'message'=>'Registro deletado com sucesso.'];
    }
}