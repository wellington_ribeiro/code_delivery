<?php

use CodeDelivery\Models\Client;
use CodeDelivery\Models\User;
use Illuminate\Database\Seeder;

class ProductPicturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')
            ->update(['picture' => "http://lorempixel.com/100/130/food/"]);
//            ->update(['picture' => "http://www.kaleidosblog.com/tutorial/kaleidosblog.png"]);
    }
}
