angular.module('starter.controllers')
  
  .controller('HomeCtrl',['$scope','$state','$http', function ($scope,$state,$http) {

    $scope.user = {};

    $http.get("http://localhost:8000/api/authenticated")
      .success(function (data) {
        $scope.user = data;
      })
      .error(function (data) {
        console.log(data);
      });

  }]);
