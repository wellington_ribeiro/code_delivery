angular.module('starter.controllers')
  
  .controller('LoginCtrl',['$scope','OAuth', '$ionicPopup', '$state', function ($scope,OAuth,$ionicPopup,$state) {

    $scope.user = {
      username: '',
      password: ''
    };

    $scope.login = function() {
      OAuth.getAccessToken($scope.user)
        .then(function (response) {
          $state.go('home')
        },function (error) {
          $ionicPopup.alert({
            title: 'Erro',
            template: 'Usuário ou senha inválidos'
          })
        });
    };
  
  }]);
