angular.module('starter.controllers')
  .controller('ClientCheckoutCtrl',[
    '$scope','$state','appConfig','$resource','$cart', function ($scope,$state,appConfig,$resource,$cart) {

      var cart = $cart.get();
      
      $scope.items = cart.items;
      $scope.total = cart.total;

  }]);
