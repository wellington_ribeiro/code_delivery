angular.module('starter.controllers')
  .controller('ClientViewProductsCtrl',[
    '$scope','$state','Product','$ionicLoading','$cart', function ($scope,$state,Product,$ionicLoading,$cart) {

      $scope.products = [];
      $ionicLoading.show({
        template: 'Carregando...'
      });
      Product.query({},function (data) {
        $scope.products = data.data;
        $ionicLoading.hide();
      },function (error) {
        console.log(error);
        $ionicLoading.hide();
      });

      $scope.addItem = function (item) {
        item.amount = 1;
        $cart.addItem(item);
        $state.go('client.checkout');
      };

  }]);
