angular.module('starter.services')
  .service('$cart',['$localStorage', function ($localStorage) {

    var key = 'cart';
    var cart = $localStorage.getObject(key) || init();

    function init(){
      return $localStorage.setObject(key,{
        items: [],
        total: 0
      });
    }

    this.clear = function () {
      init();
    };

    this.get = function () {
      return $localStorage.getObject(key);
    };

    this.getItem = function (index) {
      return this.get().items[index];
    };

    this.addItem = function (item) {
      var cart = this.get();
      var addedItem, exists;
      for (var index in cart.items) {
        addedItem = cart.items[index];
        if (addedItem.id == item.id) {
          addedItem.amount += item.amount;
          addedItem.subtotal = calculeSubTotal(addedItem);
          exists = true;
          break;
        }
      }
      if(!exists){
        item.subtotal = calculeSubTotal(item);
        cart.items.push(item);
      }
      cart.total = calculeTotal(cart.items);
      $localStorage.setObject(key,cart);
    };

    this.removeItem = function (index) {
      var cart = this.get();
      cart.items.slice(index,1);
      cart.total = calculeTotal(cart.items);
      $localStorage.setObject(key,cart);
    };

    function calculeSubTotal(item) {
      return item.price * item.amount;
    }

    function calculeTotal(items) {
      var sum = 0;
      angular.forEach(items,function (item) {
        sum += item.subtotal;
      });
      return sum;
    }

  }]);
