<div class="form-group">
    {!! Form::label('client_id','Cliente:') !!}
    {!! Form::select('client_id', $clients, null, ['class'=>'form-control chosen-select-deselect','placeholder' => 'Selecione um cliente']) !!}
</div>