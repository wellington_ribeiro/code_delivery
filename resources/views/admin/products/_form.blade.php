<div class="form-group">
    {!! Form::label('name','Nome:') !!}
    {!! Form::text('name', null, ['class'=>'form-control']) !!}
    {!! Form::label('category_id','Categoria:') !!}
    {!! Form::select('category_id', $categories, null, ['class'=>'form-control chosen-select-deselect','placeholder' => 'Selecione uma categoria']) !!}
</div>