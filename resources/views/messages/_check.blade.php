@if(Session::has('message'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">×</span><span class="sr-only">Fechar</span>
                </button>
                <strong>{{Session::get('message')}}</strong>
            </div>
        </div>
    </div>
@endif
@if(Session::has('error'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">×</span><span class="sr-only">Fechar</span>
                </button>
                <strong>{{Session::get('error')}}</strong>
            </div>
        </div>
    </div>
@endif