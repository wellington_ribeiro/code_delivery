@extends('app')

@section('content')
    <div class="container">
        <h3>Novo pedido</h3>
        </br>

        @include('errors._check')

        <div class="container">
            {!! Form::open(['route'=>'customer.orders.store','class'=>'form']) !!}

            <div class="form-group">
                <label>Total: </label>
                <p id="total"></p>
                <a href="#"id="btn-new-item" class="btn btn-default">Novo item</a>
                </br></br>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td>Produto</td>
                            <td>Quantidade</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <select class="form-control" name="items[0][product_id]">
                                    @foreach($products as $product)
                                        <option value="{{$product->id}}" data-price="{{$product->price}}">{{$product->name}} --- {{$product->price}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                {!! Form::text('items[0][amount]',1,['class'=>'form-control']) !!}
                            </td>
                        </tr>
                    </tbody>
                    <input type="hidden" value="teste" name="teste">
                </table>
            </div>


            <div class="form-group">
                {!! Form::submit('Fechar pedido',['class'=>'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>

    </div>
@endsection

@section('post-script')
    <script>
        $('#btn-new-item').click(function () {
            var row = $('table tbody > tr:last');
            var new_row = row.clone();
            var length = $('table tbody tr').length;

            new_row.find('td').each(function () {
                var td = $(this);
                var input = td.find('input,select');
                var name = input.attr('name');

                input.attr('name', name.replace((length - 1) + "", length + ""));
            });

            new_row.find('input').val(1);
            new_row.insertAfter(row);
        });

        $(document.body).on('click','select',function () {
            calculateTotal();
        });

        $(document.body).on('blur','input',function () {
            calculateTotal();
        });

        function calculateTotal() {
            var total = 0;
            var rows = $('table tbody tr');
            var row = null, price, amount;

            for (var i = 0; i < rows.length; i++) {
                row = rows.eq(i);
                price = row.find(':selected').data('price');
                amount = row.find('input').val();
                total += price * amount;
            }

            $('#total').html(total);
        }
    </script>
@endsection